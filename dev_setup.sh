#!/bin/bash
echo "What OS?";
read OS;
os=$OS | tr [A-Z] [a-z];
echo $os;

if [[ ! -d ~/.config/nvim/colors/ ]]; then
	mkdir ~/.config/nvim/colors/
fi

if [[ ! -f ~/.bashrc ]]; then
	touch ~/.bashrc
fi

if [ $os = "ubuntu" ]; then
	dpkg -s git &> /dev/null
	if [ $? -eq 0 ]; then
    	echo "Git is installed!"
	else
		sudo apt install git
	fi
fi

if [[ ! -d ~/.config/nvim/bundle ]]; then
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.config/nvim/bundle/Vundle.vim
fi

ln -sf ~/repos/server-dotfiles/molokai.vim ~/.config/nvim/colors/molokai.vim
ln -sf ~/repos/server-dotfiles/vimrc.main ~/.config/nvim/init.vim
ln -sf ~/repos/server-dotfiles/tmuxConfig.main ~/.tmux.conf
ln -sf ~/repos/server-dotfiles/bashrc.main ~/.bashrc

source ~/.bashrc
