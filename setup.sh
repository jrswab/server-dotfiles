#!/bin/bash
# To-Do: Make this distro agnostic!
#sudo apt-get -y install zsh
#chsh -s $(which zsh)
#sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

if [[ ! -d ~/.config/nvim/colors/ ]]; then
	#sudo apt-get -y install software-properties-common
	#sudo apt-add-repository ppa:neovim-ppa/stable
	#sudo apt-get -y update
	#sudo apt-get -y install neovim
	#sudo apt-get -y install python-dev python-pip python3-dev python3-pip
	mkdir ~/.config/nvim/colors/
fi

if [[ ! -f ~/.bashrc ]]; then
	touch ~/.bashrc
fi

if [[ ! -f ~/.zshrc ]]; then
	touch ~/.zshrc
fi

#dpkg -s git &> /dev/null
#if [ $? -eq 0 ]; then
#   	echo "Git is installed!"
#else
#	sudo apt install git
#fi

if [[ ! -d ~/.config/nvim/bundle ]]; then
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.config/nvim/bundle/Vundle.vim
fi

ln -sf ~/repos/server-dotfiles/vimrc.main ~/.config/nvim/init.vim
ln -sf ~/repos/server-dotfiles/tmuxConfig.main ~/.tmux.conf
ln -sf ~/repos/server-dotfiles/bashrc.main ~/.bashrc
ln -sf ~/repos/server-dotfiles/zshrc.main ~/.zshrc
ln -sf ~/repos/server-dotfiles/dircolors.256dark ~/.dir_colors
echo "Links created..."
echo ""
